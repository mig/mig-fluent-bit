# do not strip the binary
%global __strip /bin/true
%global debug_package %{nil}

# extract version information from the "sources" file
%define src_url     %((grep -h /fluent-bit/ %{_sourcedir}/sources %{_builddir}/sources) \
                    2>/dev/null | perl -ane 'print("$F[1]")')
%define src_version %(echo %{src_url} | perl -pe 's{.+/tags/v}{}; s{\\.tar\\.gz$}{}')

# cmake oddities
%if 0%{?rhel} == 7
%define cmake cmake3
%else
%define cmake cmake
%endif

Name:           mig-fluent-bit
Version:        %{src_version}
Release:        1%{?dist}
Summary:        Fast data collector for Linux
Group:          System Environment/Daemons
License:        Apache-2.0
URL:            https://fluentbit.io/
Source0:        v%{version}.tar.gz
Source1:        sources
BuildRequires:  gcc, gcc-c++, flex, bison, %{cmake}, perl
BuildRequires:  libyaml-devel, openssl-devel, systemd-devel, zlib-devel

%description
Fluent Bit is a high performance and multi platform log processor & forwarder.

%prep
%setup -q -n fluent-bit-%{version}

%build
cd build
%{cmake} \
  -DCMAKE_INSTALL_PREFIX=/usr \
  -DCMAKE_INSTALL_SYSCONFDIR=/etc \
  -DFLB_DEV=On \
  -DFLB_JEMALLOC=On \
  -DFLB_IN_DOCKER=Off \
  -DFLB_IN_DOCKER_EVENTS=Off \
  -DFLB_IN_MQTT=Off \
  -DFLB_FILTER_AWS=Off \
  -DFLB_FILTER_KUBERNETES=Off \
  -DFLB_OUT_CLOUDWATCH_LOGS=Off \
  -DFLB_OUT_KAFKA=Off \
  -DFLB_OUT_KAFKA_REST=Off \
  -DFLB_OUT_PGSQL=Off \
  ..
%make_build
# debug information
file bin/fluent-bit
ldd bin/fluent-bit
bin/fluent-bit -h

%install
rm -fr ${RPM_BUILD_ROOT}
cd build
install -d             ${RPM_BUILD_ROOT}/etc/fluent-bit
install -d             ${RPM_BUILD_ROOT}/usr/sbin
install bin/fluent-bit ${RPM_BUILD_ROOT}/usr/sbin/fluent-bit

%files
%defattr(-,root,root,-)
/etc/fluent-bit
/usr/sbin/fluent-bit
